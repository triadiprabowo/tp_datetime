// dependencies
var express = require('express'),
	http = require('http'),
	modRewrite = require('connect-modrewrite'),
	compression = require('compression'),
	app = express();

// sys env
var processEnv = require('./env').environment;
global.srv = require('./local_serve')[processEnv];
global.$root = __dirname;

// req. middleware
app.use(compression({level: 9}));

// rewrite uri statement 
app.use(modRewrite([
	'!^/api/.*|\\_getModules|\\.html|\\.js|\\.css|\\.swf|\\.jp(e?)g|\\.png|\\.gif|\\.svg|\\.eot|\\.ttf|\\.woff|\\.pdf$ /index.html [L]'    
]));

// public dir
app.use(express.static('./dist/'));

// serve app
http.createServer(app).listen(srv.port, function() {
	console.log('HTTP Server started on localhost port '+srv.port);
});