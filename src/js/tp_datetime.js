/*
** Triadi Prabowo DateTime Plugin for jQuery 1.12.3
** @author: Triadi Prabowo
** @URI: https://bitbucket.org/triadiprabowo/tp-datetime
*/
(function($) {
	// set init settings and vars
	var settings = {
		readOnly: true,
		shortMonth: true,
		minYear: 1970,
		format: 'YYYY-MM-DD HH:mm:ss',
		maxYear: new Date().getFullYear(),
		allowFuture: true,
		allowPast: true,
		currentDate: new Date(),
		dateOnly: false
	}

	var dataList = {
		months: !settings.shortMonth? ['January', 'February', 'March', 'April', 'May', 
			'June', 'July', 'August', 'September', 'October', 'November', 'December'] :
			['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
		days: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],

	}

	var _tp_selectedDate = null;
	var _tp_selectedMonth = settings.currentDate.getMonth();
	var _tp_selectedYear = settings.currentDate.getFullYear();
	var _tp_selectedTime = '00:00:00';

	// set default selected date	
	_tp_selectedDate = _tp_selectedDate? _tp_selectedDate : new Date().getDate();

	var _tp_onChange = {
		monthYear: function(e, srcdata, opts) {
			var $this = this;

			$('.tp_datetime-year').unbind('change').bind('change', function() {
				_tp_selectedYear = parseInt(this.value);
				return $this.re_render(srcdata, e, opts);
			});

			$('.tp_datetime-month').unbind('change').bind('change', function() {
				_tp_selectedMonth = parseInt(this.value);
				return $this.re_render(srcdata, e, opts);
			});
		},
		hourMin: function(e, opts) {
			$('.tp_datetime-hour, .tp_datetime-min').unbind('change').bind('change', function() {
				_tp_selectedTime = $('.tp_datetime-hour').val()+':'+$('.tp_datetime-min').val()+':00'
				_tp_setValue(e, opts);	
			});
		}
	}

	var _tp_onClick = {
		activeBlock: function(e, opts) {
			$('.tp_datetime-active').unbind('click').bind('click', function() {
				$('.tp_datetime-active').removeClass('selected');
				$(this).addClass('selected');
				_tp_selectedDate = $(this).text();
				_tp_setValue(e, opts);
			});
		}
	}

	var _tp_setValue = function(e, opts) {
		var val = _tp_selectedYear+'-'+(_tp_selectedMonth+1 < 10? '0'+
			(_tp_selectedMonth+1) : _tp_selectedMonth+1)+'-'+_tp_selectedDate+' '+_tp_selectedTime;

		$('.tp_datetime-outer').remove();
		return $(e).val(moment(Date.parse(val)).format(opts.format));
	}

	var _render = {
		wrapper: function() {
			return '<div class="tp_datetime-outer"><div class="tp_datetime-wrapper">'
		},
		header: function(data, settings) {
			var tpl = '<table class="tp_datetime-container">\
						<thead>\
							<th colspan="4">\
								<select class="tp_datetime-year">';

			for(var i=settings.maxYear; i > (settings.minYear-1); i--) {
				tpl += '<option value="'+i+'"'+(_tp_selectedYear == i? 'selected' : '')+'>'+i+'</option>';

				i-1 == settings.minYear-1? (tpl += '</select></th><th colspan="3"><select class="tp_datetime-month">') : ''
			}

			var currentYear = new Date().getFullYear(); 
			var currentMonth = new Date().getMonth()
			var maxMonth = null;

			if(_tp_selectedYear >= currentYear && !settings.allowFuture) {
				maxMonth = (data.months.length - currentMonth) + 1;
			}
			else {
				maxMonth = data.months.length;
			}

			for(var i=0; i < maxMonth; i++) {
				tpl += '<option value="'+i+'"'+(_tp_selectedMonth == i? 'selected' : '')+'>'+data.months[i]+'</option>';

				i+1 == data.months.length? (tpl += '</select></th></thead>') : '';
			}			

			return tpl;
		},
		body: function(data, settings) {
			var tpl = '<tbody class="tp_datetime-body"><tr>',
				dates = [];

			for(var i=0; i < data.days.length; i++) {
				tpl += '<td>'+data.days[i]+'</td>';

				i+1 == data.days.length? (tpl += '</tr>') : '';
			}

			if(_tp_selectedYear % 4 == 0 || _tp_selectedYear % 100 == 0 || _tp_selectedYear % 400 == 0) {
				dates = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
			}
			else {
				dates = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
			}

			var monthStartDay = new Date(_tp_selectedYear, _tp_selectedMonth, 1).getDay(),
				monthEndDay = new Date(_tp_selectedYear, _tp_selectedMonth + 1, 0).getDay();

			var curr = 1, prev = (dates[(_tp_selectedMonth-1 < 0? 11 : (_tp_selectedMonth-1))] - monthStartDay) + 1, limitLoop = dates[_tp_selectedMonth]+monthStartDay, ftr = 1;

			console.log(prev)

			limitLoop = (limitLoop % 7 === 0? limitLoop : limitLoop += (7 - (limitLoop % 7)));

			for(var i=0; i < limitLoop; i++) {
				if(i % 7 === 0) {
					tpl += '<tr>';

					if(i >= monthStartDay) {
						curr > dates[_tp_selectedMonth]? (tpl += '<td class="tp_datetime-disabled">'+ (ftr++) + '</td>') : 
							(tpl += '<td class="tp_datetime-active'+(curr == _tp_selectedDate? ' selected':'')+'">'+ (curr++) + '</td>');
					}
					else if(i < monthStartDay) {
						tpl += '<td class="tp_datetime-disabled">' + (prev++) + '</td>';
					}
				}
				else {
					if(i >= monthStartDay) {
						curr > dates[_tp_selectedMonth]? (tpl += '<td class="tp_datetime-disabled">'+ (ftr++) + '</td>') : 
							(tpl += '<td class="tp_datetime-active'+(curr == _tp_selectedDate? ' selected':'')+'">'+ (curr++) + '</td>');
					}
					else if(i < monthStartDay) {
						tpl += '<td class="tp_datetime-disabled">' + (prev++) + '</td>';					
					}
				}	
			}

			if(!settings.dateOnly) {
				tpl += '<tr><td colspan="4">Hour <select class="tp_datetime-hour">';						

				for(var i=0; i < 24; i++) {
					i+1 == 25? (tpl += '<option value="'+(i < 10? '0'+i : i)+'">'+(i < 10? '0'+i : i)+'</option></select>') : 
						(tpl += '<option value="'+(i < 10? '0'+i : i)+'">'+(i < 10? '0'+i : i)+'</option>');
				}

				tpl += '<td colspan="3">Min <select class="tp_datetime-min">';

				for(var i=0; i < 60; i++) {
					i+1 == 61? (tpl += '<option value="'+(i < 10? '0'+i : i)+'">'+(i < 10? '0'+i : i)+'</option></select>') : 
						(tpl += '<option value="'+(i < 10? '0'+i : i)+'">'+(i < 10? '0'+i : i)+'</option>')
				}
			}			

			return tpl;
		},
		all: function(e, data, settings) {
			if($('.tp_datetime-outer').length > 0) {
				$('.tp_datetime-outer').remove();
			}

			var $this = this;

			setTimeout(function() {
				var compiled = $this.wrapper() + $this.header(data, settings);
				compiled += $this.body(data, settings);

				$(e).parent().append(compiled);

				var tmpHour = _tp_selectedTime.substr(0,2), tmpMin = _tp_selectedTime.substr(3,2);
				$('.tp_datetime-hour').val(tmpHour);
				$('.tp_datetime-min').val(tmpMin);

				$('.tp_datetime-wrapper').css({
					left: $(e).position().left
				});

				if(_tp_selectedMonth != new Date().getMonth()) {
					$('.tp_datetime-active').removeClass('selected');
				}				

				// onChange hour and min handler
				_tp_onChange.hourMin.apply($this, [e, settings]);

				// onChange month and year handler
				_tp_onChange.monthYear.apply($this, [e, dataList, settings]);

				// onClick calendar active block
				_tp_onClick.activeBlock.apply($this, [e, settings]);
			}, 10);			
		},
		re_render: function(data, e, opts) {
			if(_tp_selectedMonth != new Date().getMonth() || 
				_tp_selectedYear != new Date().getFullYear()) {

				$('.tp_datetime-active').removeClass('selected');
			}

			$('.tp_datetime-container').empty();			
			$('.tp_datetime-container').append(this.header(data, opts) + this.body(data, opts));					

			// onChange month and year handler
			_tp_onChange.monthYear.apply(this, [e, dataList, opts]);

			// onClick calendar active block
			_tp_onClick.activeBlock.apply(this, [e, opts]);	
		}
	}	

	$.fn.tp_datetime = function(options) {
		if(typeof moment == "undefined") return console.error("Moment.js must be loaded to use this plugin");

		var $this = this;				

		return this.each(function(i) {			
			var opts = $.extend({}, settings, options || {});

			var _elem = $(this);

			_elem.attr({
				readonly: settings.readOnly
			});

			_elem.unbind('click').bind('click', function(e) { 
				_clickedElem = $(this);

				// element binding on click				
				$(document).on('click', function(evt) {
					evt.preventDefault();

					if(!$(evt.target).closest($this.selector+', .tp_datetime-wrapper').length) {
						return $('.tp_datetime-outer').remove()
					}

					return true;
				});

				return (function() {
					if(!_clickedElem.val()) {
						_tp_selectedMonth = opts.currentDate.getMonth();
						_tp_selectedYear = opts.currentDate.getFullYear();
						_tp_selectedDate = new Date().getDate();
						_tp_selectedTime = '00:00:00'
					}
					else {
						var tempDate = new Date(_clickedElem.val());
						_tp_selectedMonth = tempDate.getMonth();
						_tp_selectedYear = tempDate.getFullYear();
						_tp_selectedDate = tempDate.getDate();
						_tp_selectedTime = moment(tempDate).format('HH:mm:ss')
					}

					return _render.all(_clickedElem, dataList, opts);					
				})();				
			});
		});		
	}
}(jQuery));