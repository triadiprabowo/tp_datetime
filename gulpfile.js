/*
** Gulpfile Configuration
** @author Triadi Prabowo
*/

var gulp = require('gulp'),
	cssmin = require('gulp-cssmin'),
	uglify = require('gulp-uglifyjs'),
	nop = require('gulp-nop'),
	runSequence = require('run-sequence'),
	del = require('del'),
	rename = require('gulp-rename'),
	htmlmin = require('gulp-htmlmin'),
	task = [];

// Task and process type
// development -> compile & run local server
// production -> compile only
var processType = require('./env').environment;

// Gulp tasks
if(processType == 'development') {
	task = ['css', 'jsMain', 'jsVendor', 'index', 'watch'];

	var nodemon = require('gulp-nodemon');
}
else {
	task = ['css', 'jsMain', 'jsVendor', 'index'];
}

// PATH variables
var path = {
	css: './src/css/',
	dist: './dist/',
	js: './src/js/',
	src: './src/'
};

gulp.task('del', function(e) {
	return del(path.dist);
});

gulp.task('css', function() {
	return gulp
		.src(path.css+'*')
		.pipe(cssmin())
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest(path.dist+'css/'));
});

gulp.task('jsMain', function() {
	return gulp
		.src(path.js+'*')
		.pipe(processType == 'development'? nop() : uglify())
		.pipe(gulp.dest(path.dist+'js/'))
});

gulp.task('jsVendor', function() {
	return gulp
		.src(path.src+'vendor/*')
		.pipe(gulp.dest(path.dist+'js/'))	
})

gulp.task('index', function() {
	return gulp
		.src(path.src+'index.html')
		.pipe(processType == 'development'? nop() : htmlmin({collapseWhitespace: true}))
		.pipe(gulp.dest(path.dist))
});

gulp.task('watch', function() {	
	gulp.watch(path.css+'*', ['css']);	
	gulp.watch(path.js+'*', ['jsMain']);
	gulp.watch(path.src+'index.html', ['index']);
});

gulp.task('devServe', function() {
	return nodemon({
		script: 'app.js'
	});
});

// Set gulp task 'default'
// Production compile only
// Development compile & run development server
if(processType == 'development') {
	gulp.task('default', function(e) {
		runSequence('del', task, 'devServe', function() {
			console.log('['+processType+'] Application contents streamed successfully');
		});
	});
}
else {
	gulp.task('default', function(e) {
		runSequence('del', task, function() {
			console.log('['+processType+'] Application contents streamed successfully');
		});
	});
}