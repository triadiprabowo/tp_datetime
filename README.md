# TP Datetime Picker for jQuery #

##**Installation**##

```
#!html

<link rel="stylesheet" href="/dist/css/tp_datetime.min.css">

<script src="/dist/js/jquery.min.js"></script>
<script src="/dist/js/moment.min.js"></script>
<script src="/dist/js/tp_datetime.js"></script>
```

##**Usage**##

```
#!javascript

$(element).tp_datetime(options);
```

##**Options**##
|     name    |       default       |                             description                            |
|:-----------:|:-------------------:|:------------------------------------------------------------------:|
| readOnly    | true                | Make readonly to text input                                        |
| shortMonth  | true                | Make months name short or long (e.g Jan / January)                 |
| minYear     | 1970                | Minimum year                                                       |
| format      | YYYY-MM-DD HH:mm:ss | Default format after click on calendar (see moment.js format docs) |
| dateOnly    | false               | Show date and time picker or date only if set to true              |
| currentDate | new Date()          | Default of current selected date                                   |